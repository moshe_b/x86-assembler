#include "dataStructures.h"
#include "parseBases.h"

/*
convert to binary number in base 10

input: numToConvert - the number to parse to binary
ouput: base2 - the binary number
*/
char* convertToBase2(int numToConvert)
{
	char* base2 = 0;
	int i = 0;
	int first1Flag = 0;

	base2 = (char*)calloc(sizeof(char),SIZE_WORD);

	if (numToConvert < 0) /*encoding negetive number*/
	{
		for (; i < 24; i++)
		{
			base2[23 - i] = (numToConvert & 1 << i) ? '1' : '0';
			if (base2[i] == '1' && !first1Flag)
				first1Flag = 23 - i;
		}
	}
	else
	{
		for (; i < 24; i++)
			base2[23 - i] = (numToConvert & 1 << i) ? '1' : '0';
	}
	return base2;
}

/*
convert binary word of 24
bits to base 16.

input: base2 - the binary number
output: base16 - the converted number in base 16
*/
char* Base2ToBase16(char* base2)
{
	char* base16 = 0;
	const char digits[] = "0123456789abcdef";
	char currNum[5] = { 0 };
	int i = 0;
	int j = 0;
	int num = 0;

	base16 = (char*)malloc(sizeof(char) * 7);

	for (i = 0, j = 0; i < SIZE_WORD; i+=4,j++)
	{
		strncpy(currNum, base2 + i, 4);
		num = (currNum[0] - ZERO_ASCII_VAL) * pow(2, 3) +
			  (currNum[1] - ZERO_ASCII_VAL) * pow(2, 2) +
			  (currNum[2] - ZERO_ASCII_VAL) * pow(2, 1) +
			  (currNum[3] - ZERO_ASCII_VAL) * pow(2, 0);
		base16[j] = digits[num];
	}
	base16[j] = 0; /*make him string*/
	return base16;
}
