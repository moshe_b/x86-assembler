/*
this source file have
all the helping functions
for checking if the input
is valid.
*/

#include"checkValidInputHelpFunctions.h"
#include"dataStructures.h"
#include"InputAnalysis.h"
#include"encodingToDataStructures.h"
#include"addingToDataStructures.h"
#include"parseBases.h"


/*
check if val string is in arr that is 
2d array/array of strings

input: arr - 2d array of stirngs
	   val - the string to search in 
	   the array of strings
output: true - val is in arr
		false - otherwise
*/
int nameInArrayChar(char** arr, char* val)
{
	int i = 0;
	for (; arr[i][0] != END_OF_WORDS_SIGN; i++)
	{
		if (!strcmp(val, arr[i]))
			return true;
	}

	return false;
}

/*
check if number is is in array
of numbers.

input: arr - array of numbers
	   val - the number to check
	   if he in the array
output: true - val is in array
		false - otherwise
*/
int nameInArrayInt(int* arr, int val)
{
	int i = 0;
	for (; arr[i] != -1; i++)
		if (val == arr[i])
			return true;
	return false;
}

/*
create array of indexes of all
the indexes of text in splitedRow

input: splitedRow -array of strings
	   text - string to find all his indexes in the 2d array
output: indexes - the array of indexes
*/
int* indexesText(char** splitedRow, char* text)
{
	int i = 0;
	int* indexes = 0;
	int indINdexes = 0;

	indexes = (int*)malloc(sizeof(int) * SIZE_INDEXES);

	/*find all index of text*/
	for (; splitedRow[i][0] != '%'; i++) /*run until end of array*/
	{
		if (!strcmp(text, splitedRow[i]))
		{
			indexes[indINdexes] = i;
			indINdexes++;
		}
	}

	indexes[indINdexes] = -1;
	return indexes;
}

/*
find all indexes of the none
comma and lable

input: splitedRow - the splitedRow
output: indexes - array of all the indexes
		of the none comma and lable in the string
*/
int* indexesNoneCommaNoneLable(char** splitedRow)
{
	int i = 0;
	int* indexes = 0;
	int indNameCommand = 0;
	int indIndexes = 0;

	indexes = (int*)malloc(sizeof(int) * SIZE_INDEXES);
	indNameCommand = nameCommandIndex(splitedRow);

	/*searching for none comma or lable for 
	adding them to teh list*/
	for (; splitedRow[i][0] != END_OF_WORDS_SIGN; i++)
	{
		/*chekc that it's not comma or not lable(ending with :)*/
		if (strcmp(splitedRow[i], ",") && splitedRow[i][strlen(splitedRow[i]) - 1] != ':' 
			&& i != indNameCommand && splitedRow[i][0] != '"' && 
			splitedRow[i][strlen(splitedRow[i])-1] != '"') /*find all none commas lables and the name of command*/
		{
			indexes[indIndexes] = i;  /*adding the index*/
			indIndexes++;
		}
	}

	indexes[indIndexes] = -1;

	return indexes;
}

/*
return the length of the array
of numbers. the end of array is
end with -1.

input: arr - array to get his length
output: count - the length of the array 
*/
int lenArr(int* arr)
{
	int count = 0;
	for (; arr[count] != -1; count++);
	return count;
}

/*
the funnction check if there is number
in the array that is between min and max.

input: arr - array of numbers
	   min - the bootom range 
*/
int existInRange(int arr[], int min, int max)
{
	int i = 0;
	/*check if value in array exist between max to min*/
	for (; arr[i] != -1 && (arr[i] < min || arr[i] > max); i++);
	return (arr[i] != -1) ? true : false;
}

char* sliceStr(char* str, int min, int end)
{
	char* retValStr = 0;
	
	retValStr = (char*)malloc(sizeof(char)*20);
	strncpy(retValStr, str + min, end - min);

	return retValStr;
}

void appendDataToArray(int arr[], int numToAdd)
{
	int i = -1;
	while (arr[++i] != -1);
	arr[i] = numToAdd;
	arr[i + 1] = -1;
}

void appendTextToArray(char errorsArr[80][200], char* addData, int rowCounter)
{
	int i = 0;
	char rowNum[80] = {0};
	sprintf(rowNum, "%d. ", rowCounter);
	while (errorsArr[i++][0] != '%');
	strcat(rowNum, addData);
	strcpy(errorsArr[i - 1], rowNum);
	errorsArr[i][0] = '%'; 
}

/*
finding the name command index
in the enum of the saving words.
*/
int findNameCommand(char** splitedRow)
{
	int i = 0;
	int j = 0;
	
	while (splitedRow[i][0] != END_OF_WORDS_SIGN)
	{
		for (j = 0; savingWords[j][0] != END_OF_WORDS_SIGN; j++)
		{
			if (!strcmp(splitedRow[i], savingWords[j]))
				return j; /*return the number of the word*/
		}

		i++;
	}

	return -1; /*there is no such command*/
}

/*
find the index of the name commnad
in the splited words

input: splitedRow - array of strings that
	   contain the splited row from the assambly file
output: None
*/
int nameCommandIndex(char** splitedRow)
{
	int i = 0;
	int j = 0;

	while (splitedRow[i][0] != END_OF_WORDS_SIGN)
	{
		for (j = 0; savingWords[j][0] != END_OF_WORDS_SIGN; j++)
		{
			if (!strcmp(splitedRow[i], savingWords[j]))
				return i; /*return the number of the word*/
		}

		i++;
	}

	return -1; /*there is no such command*/
}

/*
check if string is number.

input: str - the string for checking
output: 1 - the string is number
		0 - otherwise
*/
int strIsNum(char str[])
{
	int i = 0;
	for (; i < strlen(str); i++)
		if (!isdigit(str[i]))
			return 0;
	return 1;
}


/*
check the valid of parameter for 
two parameters command.

input: param - the parameter
output: methodeAdress - 
		-1 -> something is not correct
		with parameter
		other number - number between 0 to 3 
		without 2 that say the number of the methode
		address
*/
int checkValidParamForTwoParamCommand(char* param)
{
	int methodeAdress = 0;

	/*two parameters command is not used is number 2 methode address*/
	if (param[0] == '&')
		appendTextToArray(errors, "methode address number 2 is not ilegal for this command\n",rowCounter);

	if (param[0] == '#')
	{
		methodeAdress = isNum(param+1) ? 0 : -1;
		if (methodeAdress == -1) /*after the # there is no number*/
		{
			appendTextToArray(errors, "invalid number after #",rowCounter);
		}
		goto endFunc;
	}
	if (nameInArrayChar((char**)valdiRegister, param))
		methodeAdress = 3;
	else if (param[0] == 'r')
	{
		if(!strIsNum(param + 1))
			appendTextToArray(errors, "invalid register name, must contain number after \"r\" \n",rowCounter);
		else if(atoi(param + 1) < 0 || atoi(param+1) > 7)
			appendTextToArray(errors, "invalid register name, only r1 --> r8 are allowed \n",rowCounter);
	}
	else
	{
		methodeAdress = nameInArrayChar(savingWords,param) ? -1 : 1;

		if (methodeAdress == -1) /*the lable is saving word*/
			appendTextToArray(errors, "Inavlid parameter. saving word is not allowed \n",rowCounter);
	}

	endFunc:
	return methodeAdress;
}


/*
check the valid of the two parameters
of the two parameters command and encode 
the row if everything is fine

input: splitedRow - the splited row from the assambly file
	   commandNameIndex - the index of the name command, used
	   for the data structure that contain the valid address methode
	   name
	   lableFlag - true - there is definition of a lable at the sentense
				   false - otherwise
output: true - everything is valid with the parameters
		false - otherwise
*/
int checkValid2Params(char** splitedRow, int commandNameIndex, boolean lableFlag)
{
	int srcMethodeAdress = -1;
	int destMethodeAdress = -1;
	char firstParam[100] = { 0 };
	char secondParam[100] = { 0 };
	int startIndex = 0;
	int validSrcParam = false;
	int validDstParam = false;

	startIndex = lableFlag ? 1 : 0;
	strcpy(firstParam, splitedRow[startIndex + 1]);
	strcpy(secondParam, splitedRow[startIndex + 3]);
	
	/*check the valid of the parameter(syntax only)*/
	srcMethodeAdress = checkValidParamForTwoParamCommand(firstParam);
	destMethodeAdress = checkValidParamForTwoParamCommand(secondParam);

	/*check valid of the address methods*/
	validSrcParam = nameInArrayInt(commandWordsData[commandNameIndex].srcAddressMethod,
		srcMethodeAdress) && srcMethodeAdress != -1;
	validDstParam = nameInArrayInt(commandWordsData[commandNameIndex].dstAddressMethod,
		destMethodeAdress) && destMethodeAdress != -1;
	if (!validSrcParam)
	{
		switch (destMethodeAdress)
		{
		case 0:
			appendTextToArray(errors, "number source parameter is not allowed for this command \n", rowCounter);
			break;
		case 1:
			appendTextToArray(errors, "lable source parameter is not allowed for this command \n", rowCounter);
			break;
		case 2:
			appendTextToArray(errors, "jumping source parameter (number 2 methode address) is not allowed \n", rowCounter);
			break;
		case 3:
			appendTextToArray(errors, "register as source parameter is not alllowed for this command \n", rowCounter);
			break;
		}
		return false; /*parameters are not valid, mybe the synatx or the address methodes*/
	}
	else if (!validDstParam)
	{
		switch (destMethodeAdress)
		{
		case 0:
			appendTextToArray(errors, "number as destiantion parameter is not allowed for this command \n", rowCounter);
			break;
		case 1:
			appendTextToArray(errors, "lable as destination parameter is not allowed for this command \n", rowCounter);
			break;
		case 2:
			appendTextToArray(errors, "jumping destination parameter (number 2 methode address) is not allowed \n", rowCounter);
			break;
		case 3:
			appendTextToArray(errors, "register as destination parameter is not alllowed for this command \n", rowCounter);
			break;
		}
		return false;
	}
	else
	{
		/*all data is valid and now you can encode the data, for the first pass*/
		encode2Param(srcMethodeAdress, destMethodeAdress, commandNameIndex, 
			lableFlag, firstParam, secondParam, splitedRow);
		return true;
	}
}
 
/********one parameter commands*********/


/*
check the valid of one parameter command.

input: splitedRow - the splited row from the assambly file
	   commandNameIndex - the index of the name command, used
	   for the data structure that contain the valid address methode
	   name
	   lableFlag - true - there is definition of a lable at the sentense
				   false - otherwise
output: true - everything is valid with the parameters
		false - otherwise
*/
int checkValid1ParamCommand(char** splitedRow, int commandNameIndex, boolean lableFlag)
{
	int destMethodeAdress = -1;
	int validDestParam = 0;

	/*get the address method*/
	destMethodeAdress = checkValid1Param(splitedRow[lableFlag ? 2 : 1]);
	/*check that he is valid address methode*/
	validDestParam = nameInArrayInt(commandWordsData[commandNameIndex].dstAddressMethod,
		destMethodeAdress);
	if (validDestParam && destMethodeAdress != -1)/*every thing is fine*/
	{
		/*encoding the row*/
		encode1Param(destMethodeAdress, commandNameIndex, lableFlag, splitedRow[lableFlag ? 2 : 1], splitedRow);
		return true;
	}
	else if(!validDestParam)
	{
		switch (destMethodeAdress)
		{
			case 0:
				appendTextToArray(errors, "number parameter is not allowed for this command \n",rowCounter);
				break;
			case 1:
				appendTextToArray(errors, "lable parameter is not allowed for this command \n", rowCounter);
				break;
			case 2:
				appendTextToArray(errors, "jumping parameter (number 2 methode address) is not allowed \n", rowCounter);
				break;
			case 3:
				appendTextToArray(errors, "register as parameter is not alllowed for this command \n", rowCounter);
				break;
		}
		return false;
	}
      return false;
}

/*
check the valid of the parameter
from one parameter row command

input: param - the parameter to check
	   if his is valid
output: destAdressMethode - 
		the address methode according to the parameter
		or -1 if the parameter is not valid
*/
int checkValid1Param(char* param)
{
	int destAdressMethode = -1; /*-1 mean that there is a problem with the parameter*/

	if (param[0] == '&')
		destAdressMethode = 2;
	else if (param[0] == '#')
	{
		if (!isNum(param+1))
			appendTextToArray(errors, "invalid number after signed '#' \n",rowCounter);
		else
			destAdressMethode = 0;
	}
	else if (param[0] == 'r')
	{
		if (!isNum(param+1))
			appendTextToArray(errors, "Invalid name of register, must be number after 'r' \n",rowCounter);
		else if (atoi(param + 1) < 0 || atoi(param + 1) > 7)
			appendTextToArray(errors, "Invalid number of register, must be between 0 to 7 \n",rowCounter);
		else
			destAdressMethode = 3;
	}
	else /*the parameter is a lable*/
	{
		destAdressMethode = nameInArrayChar(savingWords, param) ? 1 : -1;
		if (!destAdressMethode) /*the lable is saving word*/
			appendTextToArray(errors, "saving word as parameter is not allowed! \n",rowCounter);
		else
			destAdressMethode = 1;
	}
	return destAdressMethode;
}

/************one parameter command**************/


/*
return the command name enum

input: command - the command name to return 
	   his enum value
output: the name command enum or -1 
		if the command name don't exist
*/
int commandIntVal(char* command)
{
	if (!strcmp("mov", command)) return mov;
	if (!strcmp("cmp", command)) return cmp;
	if (!strcmp("add", command)) return add;
	if (!strcmp("sub", command)) return sub;
	if (!strcmp("lea", command)) return lea;
	if (!strcmp("clr", command)) return clr;
	if (!strcmp("not", command)) return not;
	if (!strcmp("inc", command)) return inc;
	if (!strcmp("dec", command)) return dec;
	if (!strcmp("jmp", command)) return jmp;
	if (!strcmp("bne", command)) return bne;
	if (!strcmp("jsr", command)) return jsr;
	if (!strcmp("red", command)) return red;
	if (!strcmp("prn", command)) return prn;
	if (!strcmp("rts", command)) return rts;
	if (!strcmp("stop", command)) return stop;
	return -1; /*the command is not exist*/
}

/*
casting from int to string

input: num - the number to cast
output: strNum - the string as number
*/
char* intToStr(int num)
{
	char* strNum = 0;
	int numDigs = 0;
	int numCpy = 0;
	strNum = (char*)malloc(50);
	
	numCpy = num;
	/*get teh number of digits in the number*/
	while (numCpy > 0)
	{
		numCpy /= 10;
		numDigs++;
	}

	strNum = (char*)malloc(numDigs);
	/*creat the str number*/
	while (num > 0)
	{
		strNum[numDigs - 1] = num % 10;
		num /= 10;
		numDigs--;
	}

	return strNum;
}

/*
find the index of the "string"
in the splited row 2d array

input: splitedRow - the splited row
output: i - the index of the string
		-1 - the number is not exist
*/
int findStringIndex(char** splitedRow)
{
	int i = 0;

	while (splitedRow[i][0] != END_OF_WORDS_SIGN)
	{
		if (splitedRow[i][0] == '"' && splitedRow[i][strlen(splitedRow[i]) - 1] == '"')
			return i; /*found string and return his index*/
		i++;
	}

	return -1; /*string not found*/
}


/*
the function check if the string
is a lable parameter according to the 
rules. start with letter and after it 
numbers or letters

input: str - the string to check if he is lable
output: validLable - 
		1 - it's valid lable
		0 - otherwise
*/
int isLable(char* str)
{
	int validLable = 0;
	int i = 0;

	if (isalpha(str[0]))/*first char must be alpha*/
	{
		/*sequence of letters and numbers*/
		for (i = 1; i < strlen(str) && (isalpha(str[i]) || isdigit(str[i])); i++);
		validLable = i == strlen(str);
	}
	
	/*check if the syntax of the lable is fine and he is not saving word*/
	return !nameInArrayChar(savingWords, str) &&
		!nameInArrayChar((char**)valdiRegister, str) &&
		str[0] != '#' && validLable;
}

/*
the function check if string
is number.

input: str - string to check if he is number
output: true - the string is number
		false - otherwise
*/
int isNum(char* str)
{
	int i = 0;
	if (str[i] == '-' || str[i] == '+')
		i++;
	for (; i < strlen(str) && isdigit(str[i]); i++);

	return i == strlen(str) ? true : false;
}

/*
check the valid of the sequence of
numbers in the .data instruction

input: splitedRow - the splited row
output: true - all the sequence of the 
		numbers are legal
		false - otherwise
*/
int dataNumbersEncode(char** splitedRow)
{
	int i = 0;
	int numFlag = 0; 
	int commaFlag = 0;
	int* numbers = 0;
	int numbersInd = 0;
	ROW_DATA rowData = { 0 };

	numbers = (int*)malloc(sizeof(int) * 20);

	/*
	loop trought all the numbers and commas
	*/
	while (splitedRow[i][0] != END_OF_WORDS_SIGN)
	{
		if ((isNum(splitedRow[i]) && commaFlag) || (isNum(splitedRow[i]) && i == 0))
		{
			numbers[numbersInd++] = atoi(splitedRow[i]);
			commaFlag = false;
			numFlag = true;
		}
		else if (splitedRow[i][0] == ',' && numFlag)
		{
			numFlag = false;
			commaFlag = true;
		}
		else /*sign different from number or comma or maybe it's two numbers wothout comma or two commas*/
		{
			if (commaFlag && splitedRow[i][0] == ',')
				if (splitedRow[i + 1][0] == '%') /*end of row and last number*/
					appendTextToArray(errors, "duplicate commas after last number\n",rowCounter);
				else
					appendTextToArray(errors, "duplicate commas between two numbers\n",rowCounter);
			else if (numFlag && isNum(splitedRow[i]))
				appendTextToArray(errors, "duplicate numbers without comma between them \n",rowCounter);
			else if (isNum(splitedRow[i]) && commaFlag) /*islegal number, for exmple -12a34 12b3*/
				appendTextToArray(errors, "ilegal number \n",rowCounter);
			else if (numFlag && !isdigit(splitedRow[i][0]))
				if (splitedRow[i + 1][0] == END_OF_WORDS_SIGN)
					appendTextToArray(errors, "ilegal sign after last number \n",rowCounter);
				else
					appendTextToArray(errors, "ilegal sign after number \n",rowCounter);
			else if (commaFlag && !isdigit(splitedRow[i][0]))
				appendTextToArray(errors, "ilegal text after comma \n",rowCounter);
			break; /*1 mistake in the row is enough*/
		}

		i++;
	}

	if (splitedRow[i][0] == END_OF_WORDS_SIGN) /*all the .data row is fine and legal*/
	{
		if (commaFlag)
			appendTextToArray(errors, "comma at the end of the sequense of the numbers is ilegal \n", rowCounter);
		else
		{
			rowData.wordsOfRow[0][0] = '-'; /*we don't need the words of the row to the second pass*/
			rowData.L = numbersInd;
			rowData.memoryCell = DC;
			i = 0;
			DC += numbersInd;
			for (i = 0; i < numbersInd; i++) /*convert the numbers*/
				strcpy(rowData.encode[i], convertToBase2(numbers[i]));
			rowData.rowNumber = rowCounter;
			addEncode(DATA, rowData);
			return true;
		}
	}

	return false;
}


/*
check if the string contain only 
white characters.

input: str - the string itself
output: 1 - the string is empty
		0 - otherwise
*/
int emptyString(char* str)
{
	int i = 0;
	for (; str[i] != 0 && isspace(str[i]); i++);
	return str[i] == 0;
}


/*
check if string is contain
only numbers or letters.

input: str - string to check if 
he contains only numbers

ouput: 1 - the string contain only
	   letters and numbers
	   0 - otherwise
*/
int letNumOnly(char* str)
{
	int i = 0;
	for (; i < strlen(str) && (isdigit(str[i]) || isalpha(str[i])); i++);
	return i == strlen(str);
}

