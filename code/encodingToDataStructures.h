#ifndef ENCODING_STRUCTURE
#define ENCODING_STRUCTURE

#include"dataStructures.h"
#include<stdlib.h>

extern ROW_DATA dataEncode[DATA_SIZE];
extern ROW_DATA codeEncode[CODE_SIZE];
extern SYMBOLES_TABLE_NODE* labelTable;
extern STRUCT_COMMAND commandWordsData[16];
extern char errors[80][200];
extern char valdiRegister[9][3];
extern char* savingWords[22];
extern FILE* externFile;
extern int IC, DC;


void encode2Param(int firstMethodeAddress, int secondMethodeAddress, command_words command, boolean lableFlag,
	char* firstParam, char* secondParam, char** splitedwords);
void encode1Param(int destAdressMethode, command_words command, boolean lableFlag,
	char* param, char** splitedwords);
void encodeZeroParam(char** splitedRow, boolean hasLable);
void encodeStringRow(char** splitedRow, boolean hasLable);
int addressMethodeNum(char* param);
void addEncode(char dataOrCommands, ROW_DATA add);
int addressMethodeNum(char* param);
int countNumWords(int srcMethodeAddress, int destMethodeAddress);

/*second pass*/
int updateEncodeOneParam(int index);
int updateEncodeTwoParams(int index);
#endif
