#ifndef INPUTANALYSIS
#define INPUTANALYSIS

#include<stdlib.h>
/*
functions declerations
*/
void deletSideSpaces(char** str);
char* combineAsterisk(char* rowFile);
char** splitByAsterisk(char* rowFile);

/*includes headers*/
#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>

/*consts*/
#define NUM_SPLITED_WORD 30
#define SPLITED_SIGN_STR "*"
#define SPLITED_SIGN_CHAR '*'
#define ROW_FILE_LEN 30
#define COMMA_SIGN ','
#define END_OF_WORDS_SIGN '%'

#endif
