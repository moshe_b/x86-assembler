#include<stdio.h>
#include<string.h>
#include<stdlib.h>

extern ROW_DATA dataEncode[DATA_SIZE];
extern ROW_DATA codeEncode[CODE_SIZE];
extern SYMBOLES_TABLE_NODE* labelTable;
extern char errors[80][200];
extern char valdiRegister[9][3];
extern char* savingWords[22];
extern int IC;
extern int DC;
extern int rowCounter;
extern FILE* entryFile;

int checkValidTwoParams(char** rowSplitedData);
int checkValidOneParam(char** splitedRow);
int  checkValidZeroParam(char** rowSplited);
int checkValidString(char** rowSplited);
int checkValidData(char** splitedRow);
int checkValidExtern(char** splitedRow);
int checkValidEntry(char** splitedRow);

#define COMMA_SIGN_STR ","
