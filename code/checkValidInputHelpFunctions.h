#ifndef CHECK_VALID
#define CHECK_VALID
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include"dataStructures.h"

extern char* savingWords[22];
extern STRUCT_COMMAND commandWordsData[16];
extern char errors[80][200];
extern char valdiRegister[9][3];
extern ROW_DATA dataEncode[DATA_SIZE];
extern ROW_DATA codeEncode[CODE_SIZE];
extern int IC;
extern int DC;
extern int dataRowCounter;
extern int instRowCounter;
extern int rowCounter;

int nameInArrayInt(int* arr, int val);
int nameInArrayChar(char** arr, char* val);
int* indexesText(char** splitedRow, char* text);
int* indexesNoneCommaNoneLable(char** splitedRow);
int lenArr(int* arr);
int existInRange(int arr[], int min, int max);
char* sliceStr(char* str, int min, int end);
void appendDataToArray(int arr[], int numToAdd);
int findNameCommand(char** splitedRow);
void appendTextToArray(char errorsArr[80][200], char* addData, int rowCounter);

int checkValid1Param(char* param);
int checkValidParamForTwoParamCommand(char* param);
int checkValid2Params(char** splitedRow, int commandNameIndex, boolean lableFlag);

int checkValid1ParamCommand(char** splitedRow, int commandNameIndex, boolean lableFlag);
int checkValidOneParam(char** splitedRow);
int commandIntVal(char* command);
char* intToStr(int num);
int findStringIndex(char** splitedRow);
int dataNumbersEncode(char** splitedRow);
int nameCommandIndex(char** splitedRow);
int isLable(char* str);
int emptyString(char* str);
int letNumOnly(char* str);
int isNum(char* str);

#define DATA 'd'
#define CODE 'c'
#define SIZE_INDEXES 80
#endif 

