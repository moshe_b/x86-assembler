#ifndef SECOND_PASS
#define SECOND_PASS

#include<stdio.h>
#include<stdlib.h>
#define MAX_LENGTH_ROW 81

extern ROW_DATA dataEncode[CODE_SIZE];
extern ROW_DATA codeEncode[DATA_SIZE];
extern SYMBOLES_TABLE_NODE* labelTable;
extern char errors[80][200];
extern FILE* entryFile;
extern FILE* externFile;
extern int IC, DC;

void updateSecondPass(FILE* assambllyFile);
void writingAssamblyFile(FILE* assamblyFile);

#endif 
