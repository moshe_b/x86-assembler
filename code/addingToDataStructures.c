#include "dataStructures.h"
#include "checkValidInputHelpFunctions.h"
#include "addingToDataStructures.h"


/*
insert the label to the end of the
lables table.

input: lableTbaleParam - the table table for insert
       lableToAdd - the lable for adding
output: None
*/
void insertAtEnd(SYMBOLES_TABLE_NODE** lableTableParam, SYMBOLES_TABLE_NODE* lableToAdd)
{
	SYMBOLES_TABLE_NODE* head = 0;
	SYMBOLES_TABLE_NODE* ptr = 0;

	head = *lableTableParam;
	/*lable table is empty*/
	if (!head)
	{
		*lableTableParam = lableToAdd;
		(*lableTableParam)->next = 0;
	}
	else
	{
		/*insert it in the right place for keeping the order*/
		while (head->next && lableToAdd->data.address > head->data.address) 
		{
			head = head->next;
		}

		if (head)
		{
			ptr = head->next;
			head->next = lableToAdd;
			lableToAdd->next = ptr;
		}
		else /*end of list*/
		{
			head->next = lableToAdd;
			lableToAdd->next = 0;
		}
	}
}


/*
create symbole node for the lable table
input: sign - the name of the lable
       address - the IC or CD of the lable
	   instructionOrAction - if the lable is intruction or action
	   exter - if the lable is entry or extern or not both
output: lableToAdd - the created node
*/
SYMBOLES_TABLE_NODE* createSymboleNode(char sign[], int address, int instructionOrAction, int exter)
{
	SYMBOLES_TABLE_NODE* lableToAdd = 0;
	lableToAdd = (SYMBOLES_TABLE_NODE*)malloc(sizeof(SYMBOLES_TABLE_NODE));
	strcpy(lableToAdd->data.sign, sign);
	lableToAdd->data.address = address;
	lableToAdd->data.instructOrAction = instructionOrAction;
	lableToAdd->data.exterOrEntry = exter;

	return lableToAdd;
}

/*
adding lable to the lable table 
and checking if the lable is fine.
not asving word and if his syntax is correct

input: rowSplitedData - the splited row from the
	   assambly file
output: true - the lable exist and he is in correct syntax
		false - the lable don't exist.
*/
int addLable(char** rowSplitedData)
{
	SYMBOLES_TABLE_NODE* lableToAdd = 0;
	char sign[100] = { 0 };
	int actionOrInstruct = action;
	int icDc = IC;
	char lableName[LABLE_SIZE] = { 0 };

	strncpy(lableName, rowSplitedData[0], strlen(rowSplitedData[0]) - 1);

	if (rowSplitedData[0][strlen(rowSplitedData[0]) - 1] == ':' &&
		!isspace(rowSplitedData[0][strlen(rowSplitedData[0]) - 2]) &&
		!nameInArrayChar((char**)savingWords, lableName) &&
		!nameInArrayChar((char**)valdiRegister, lableName) &&
		strlen(lableName) <= MAX_LABLE_SIZE &&
		isalpha(lableName[0]) &&
		letNumOnly(lableName))  /*there is lable and he is valid*/
	{
		
		/*the lable is intruction*/
		if (!strcmp(rowSplitedData[1],".data") || !strcmp(rowSplitedData[1],".string"))
		{
			actionOrInstruct = instruction;
			icDc = DC;
		}
		lableToAdd = createSymboleNode(strncpy(sign, rowSplitedData[0], strlen(rowSplitedData[0]) - 1),
			icDc, actionOrInstruct, notExterEntry);  /*creat node for the lable table*/
		insertAtEnd(&labelTable,lableToAdd);  /*adding the node to the table*/
		return true; /*there is a lable in the begining of the row*/
	}
	else if(rowSplitedData[0][strlen(rowSplitedData[0]) - 1] == ':' || rowSplitedData[1][0] == ':')
	{
		if (rowSplitedData[0][strlen(rowSplitedData[0]) - 1] != ':' &&  rowSplitedData[1][0] == ':')
			appendTextToArray(errors, "A colon in the definition of a label must be adjacent to the label \n",rowCounter);
		else if (nameInArrayChar(savingWords, rowSplitedData[0]) ||
			nameInArrayChar((char**)valdiRegister, rowSplitedData[0]))
			appendTextToArray(errors, "label definition is not allowed to use in saving words \n",rowCounter);
		else if (strlen(lableName) > MAX_LABLE_SIZE)
			appendTextToArray(errors, "definiton of lable length is too big, must be maximum 31 characters \n",rowCounter);
		else if (!isalpha(lableName[0]))
			appendTextToArray(errors, "lable defintion must begin with letter only \n",rowCounter);
		else if (!letNumOnly(lableName))
			appendTextToArray(errors, "lable must contain only numbers or letters \n",rowCounter);
	}
	return false; /*there is no lable at teh start of the row*/
}


/*
adding extern lable in extern sentense.

input: rowSplitedData - the splited row from the file
       lableInd - the lable index, 1 - there is no definition
	   of lable, 2 - there is.
output: true - teh lable is existing and add successfully to 
		the lable table
		false - otherwise
*/
int addLableExtern(char** rowSplitedData, int lableInd)
{
	SYMBOLES_TABLE_NODE* lableToAdd = 0;
	SYMBOLES_TABLE_NODE* head = 0;
	boolean lableAlreayExist = false;
	
	head = labelTable;

	/*checking if there was already definition 
	of a teh same exern lable*/
	while (head && !lableAlreayExist)
	{
		if (!strcmp(head->data.sign,rowSplitedData[lableInd]))
			lableAlreayExist= true;
		head = head->next;
	}
	if (lableAlreayExist) /*it's wrong to add such lable*/
	{
		appendTextToArray(errors, "definition of external lable is not allowed\n",rowCounter);
	}
	else if(!nameInArrayChar(savingWords, rowSplitedData[lableInd]) &&
		!nameInArrayChar((char**)valdiRegister, rowSplitedData[lableInd]))
	{
		lableToAdd = createSymboleNode(rowSplitedData[lableInd], 0, notExterEntry ,external); 
		insertAtEnd(&labelTable,lableToAdd);
		return true;
	}
	else
		appendTextToArray(errors, "Saving word as lable is not allowed \n",rowCounter);

	return false;
}


/*
add entry lable to the lables table

input: rowSplitedData - the splited row from the file
output: true - the lable exist and has correct syntax.
		false - otherwise
*/
int addLableEntry(char** rowSplitedData)
{
	SYMBOLES_TABLE_NODE* lableToAdd = 0;

	if (!nameInArrayChar(savingWords, rowSplitedData[0]) &&
		!nameInArrayChar((char**)valdiRegister, rowSplitedData[0]))
	{
		lableToAdd = createSymboleNode(rowSplitedData[1], 0, -1, entry);
		insertAtEnd(&labelTable,lableToAdd);
		return true;
	}
	else
		appendTextToArray(errors, "Saving word as lable is not allowed \n",rowCounter);

	return false;
}
