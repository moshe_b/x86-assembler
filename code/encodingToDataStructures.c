/*
the file will encode and save to
data structures for the second pass
*/
#include<stdio.h>
#include"dataStructures.h"
#include"parseBases.h"
#include"checkValidInputHelpFunctions.h"
#include"encodingToDataStructures.h"

/*
the function will encode
the 2 parameters row in the first pass

input: firstMethodeAddress - the methode address of the first parameter
	   secondMethodeAddress - the methode address of the second parameter
	   command - the command word enum
	   lableFlag - true or false if there lable defintion
	   firstParam - the first parameter itself
	   secondParam - the second parameter itself
	   splitedWords - all splited row
output: None
*/
void encode2Param(int firstMethodeAddress, int secondMethodeAddress, command_words command, boolean lableFlag,
	char* firstParam, char* secondParam, char** splitedwords)
{
	int numWords = 0;
	unsigned int* pBitF = 0;
	int i = 0;
	ROW_DATA rowData = { 0 };
	bitfEncoding rowEncode = { 0 };
	pBitF = (unsigned int*)(&rowEncode);


	rowEncode.sourceAddress = firstMethodeAddress;
	rowEncode.destAddress = secondMethodeAddress;
	rowEncode.funct = commandWordsData[command].fucnt != -1 ? commandWordsData[command].fucnt : 0;
	rowEncode.opcode = commandWordsData[command].opcode;

	if (firstMethodeAddress == 3) /*first param is register*/
		rowEncode.sourceRegister = atoi(firstParam+1);
	else
		rowEncode.sourceRegister = 0;

	if (secondMethodeAddress == 3) /*first param is register*/
		rowEncode.destRegister = atoi(secondParam+1);
	else
		rowEncode.destRegister = 0;

	/*zeros for the first pass, in the second pass 
	we will change it according teh lables table*/
	rowEncode.A = 1; /*always 1 in the first word*/
	rowEncode.R = 0;
	rowEncode.E = 0;
	numWords = countNumWords(firstMethodeAddress, secondMethodeAddress);
	strcpy(rowData.encode[0], convertToBase2(*pBitF));
	i = 1;
	if (firstParam[0] == '#')
	{
		strcpy(rowData.encode[1], convertToBase2(atoi(firstParam + 1)));
		for (i = 3; i < 25; i++)
			rowData.encode[1][i - 3] = rowData.encode[1][i];
		rowData.encode[1][21] = '1';
		rowData.encode[1][22] = '0';
		rowData.encode[1][23] = '0';
		i = 2;
	}
	if (secondParam[0] == '#')
	{
		strcpy(rowData.encode[2], convertToBase2(atoi(secondParam + 1)));
		for (i = 3; i < 25; i++)
			rowData.encode[2][i - 3] = rowData.encode[2][i];
		rowData.encode[2][21] = '1';
		rowData.encode[2][22] = '0';
		rowData.encode[2][23] = '0';
		i = 3;
	}
	for (; i < numWords; i++)
		rowData.encode[i][0] = '?';/*every ? is lable that will be encode in the second pass*/
	rowData.L = numWords;
	rowData.memoryCell = IC;
	IC += numWords;

	/*
	fill the parts of the row
	*/
	i = 0;
	if (lableFlag)
	{
		strcpy(rowData.wordsOfRow[0], splitedwords[0]);
		i = 1;
	}

	/*
	copy the words of the splited words
	to the data structure
	*/
	strcpy(rowData.wordsOfRow[i], splitedwords[i]);
	strcpy(rowData.wordsOfRow[i+1], splitedwords[i+1]);
	strcpy(rowData.wordsOfRow[i+2], splitedwords[i+2]);
	strcpy(rowData.wordsOfRow[i + 3], splitedwords[i + 3]);

	rowData.rowNumber = rowCounter;
	addEncode('c', rowData);
}

/*
encode 1 parameter row.

input: dastMethodeAddress - the methode address of the parameter
	   command - the command word enum
	   lableFlag - true or false if there lable defintion
	   param - parameter itself
	   splitedWords - all splited row
output: None
*/
void encode1Param(int destAdressMethode, command_words command, boolean lableFlag,
	char* param, char** splitedwords)
{
	int numWords = 1;
	unsigned int* pBitF = 0;
	int i = 0;
	ROW_DATA rowData = { 0 };
	bitfEncoding rowEncode = { 0 };
	pBitF = (unsigned int*)(&rowEncode);

	rowEncode.sourceAddress = 0;
	rowEncode.destAddress = destAdressMethode;
	if(commandWordsData[command].fucnt != -1)
		rowEncode.funct = commandWordsData[command].fucnt;
	else
		rowEncode.funct = 0;
	rowEncode.opcode = commandWordsData[command].opcode;

	rowEncode.sourceRegister = 0; /*the param in 1 param sentense is always the destination*/

	if (destAdressMethode == 3) /*first param is register*/
		rowEncode.destRegister = atoi(param + 1);
	else
		rowEncode.destRegister = 0;

	/*the first word is always 100 in the ARE*/
	rowEncode.A = 1;
	rowEncode.R = 0;
	rowEncode.E = 0;

	if (destAdressMethode != 3)
		numWords++;
	
	/*
	fill the parts of the row
	*/
	if (lableFlag) /*there is lable (LABLE:)*/
	{
		strcpy(rowData.wordsOfRow[0], splitedwords[0]); /*the lable*/
		i = 1;
	}
	strcpy(rowData.wordsOfRow[i], splitedwords[i]); /*name of command*/
	strcpy(rowData.wordsOfRow[i + 1], splitedwords[i + 1]); /*the parameter*/

	/*saving the encoding command in the "code image"*/
	strcpy(rowData.encode[0], convertToBase2(*pBitF));
	i = 1;
	if (param[0] == '#')
	{
		strcpy(rowData.encode[1], convertToBase2(atoi(param + 1)));
		puts(rowData.encode[1]);
		for (i = 3; i < 25; i++)
			rowData.encode[1][i - 3] = rowData.encode[1][i];
		rowData.encode[1][21] = '1';
		rowData.encode[1][22] = '0';
		rowData.encode[1][23] = '0';
		rowData.encode[1][24] = 0;
		i = 2;
	}
	for (; i < numWords; i++)
		rowData.encode[i][0] = '?';
	rowData.L = numWords;
	rowData.memoryCell = IC;
	IC += numWords;

	rowData.rowNumber = rowCounter;
	addEncode('c', rowData);
}

/*
add the row data including the 
encoding to dataEncoding or codeEncoding
dataStructures

input: dataOrCommands - 'C' - add to codeEncode
						'D' -  add to dataEncode
ouput: None
*/
void addEncode(char dataOrCommands, ROW_DATA add)
{
	/*static variable for saving their state
	between the calls of teh fucntions*/
	static int indexCode = 0;
	static int indexData = 0;

	if (dataOrCommands == 'd') /*data*/
	{
		dataEncode[indexData] = add;
		indexData++;
	}
	else if (dataOrCommands == 'c') /*code*/
	{
		codeEncode[indexCode] = add;
		indexCode++;
	}
	else if(dataOrCommands == 'e')/*restart for the next file*/
	{
		indexCode = 0;
		indexData = 0;
	}
}


/*
encode zero parameters sentense

input: splitedRow - the splitedRow from the assably file
	   hasLable - true or false if there is definition of lable
output: None
*/
void encodeZeroParam(char** splitedRow, boolean hasLable)
{
	unsigned int* pBitF = 0;
	int commandIndex = 0;
	bitfEncoding rowEncode = { 0 };
	ROW_DATA rowData = {0};

	commandIndex = commandIntVal(splitedRow[hasLable ? 1 : 0]);
	pBitF = (unsigned int*)(&rowEncode);

	rowEncode.A = 1;
	rowEncode.R = 0;
	rowEncode.E = 0;
	rowEncode.sourceAddress = 0;
	rowEncode.sourceRegister = 0;
	rowEncode.destAddress = 0;
	rowEncode.destRegister = 0;
	rowEncode.opcode = commandWordsData[commandIndex].opcode;
	rowEncode.funct = commandWordsData[commandIndex].fucnt != -1 ? commandWordsData[commandIndex].fucnt : 0;
    
	strcpy(rowData.encode[0], convertToBase2(*pBitF));
	rowData.L = 1;
	rowData.memoryCell = IC;
	if (hasLable)
		strcpy(rowData.wordsOfRow[0], splitedRow[0]);
	strcpy(rowData.wordsOfRow[hasLable ? 1 : 0], splitedRow[hasLable ? 1 : 0]);
	rowData.rowNumber = rowCounter;
	addEncode('c', rowData); /*adding teh row to the code image*/
	IC++; /*zero parameter row is always only 1 word*/
}

/*
encode string row

input: splitedRow - the splited row from the assambly file
	   hasLable - true or false if there is lable
	   definition in the row
output: None
*/
void encodeStringRow(char** splitedRow, boolean hasLable)
{
	int strIndex = 0;
	int i = 0;
	ROW_DATA rowData = { 0 };

	strIndex = hasLable ? 2 : 1;
	
	rowData.L = strlen(splitedRow[strIndex]) - 2 + 1; /*+1 for the null at the end of the string*/
	rowData.memoryCell = DC; /*the current DC, before teh string itself*/

	for (i = 1; i < strlen(splitedRow[strIndex]) - 1; i++)
		strcpy(rowData.encode[i - 1],convertToBase2(splitedRow[strIndex][i]));
	
	strcpy(rowData.encode[i - 1], convertToBase2(0));
	strcpy(rowData.wordsOfRow[0], splitedRow[0]);
	strcpy(rowData.wordsOfRow[1], splitedRow[1]);
	if (hasLable)
	{
		strcpy(rowData.wordsOfRow[2], splitedRow[2]);
		strcpy(rowData.wordsOfRow[3], "%");
	}
	else
		strcpy(rowData.wordsOfRow[2], "%");


	DC += rowData.L; /*update the DC to be after the string*/
	rowData.rowNumber = rowCounter;
	addEncode(DATA, rowData);
}

/*
count the number or words according
the two methode address

input: srcMethodeAddress - the source methode address
       destMethodeAddress - the dest methode address
output: count - the number of words in the row
*/
int countNumWords(int srcMethodeAddress, int destMethodeAddress)
{
	int count = 1;

	switch (srcMethodeAddress)
	{
		case 0: case 1:case 2:
			count += 1;
			break;
		case 3:
			count += 0;
	}

	switch (destMethodeAddress)
	{
		case 0: case 1:case 2:
			count += 1;
			break;
		case 3:
			count += 0;
	}

	return count;
}

/*
the function encode all the lables 
in the second pass

input: paramInd - the parameter index, 1 or 2 according
	   which parameter we want to check
	   indexRow - the index row
	   hasLable - true or false according if there is 
				  definition of a lable
ouput: true - the lable exist if the paremeter is lable
	   false - the lable don't exist
*/
int getLableAddress(int paramInd, int indexRow, boolean hasLable)
{
	int originParamInd = 0; /*which word of encode his the lable*/
	int lableIndex = 0;
	int i = 0;
	int lableOffset = 0; /*if there is & before the lable*/
	SYMBOLES_TABLE_NODE* head = 0;

	originParamInd = paramInd;
	lableIndex = hasLable ? 2 : 1;
	lableIndex = paramInd == 1 ? lableIndex: lableIndex + 2;
	head = labelTable;

	if (originParamInd == 2 && 
		!isLable(codeEncode[indexRow].wordsOfRow[lableIndex - 2]) &&
		codeEncode[indexRow].wordsOfRow[lableIndex - 2][0] != '#') /*check if the first param is lable*/
		originParamInd--;


	if(!nameInArrayChar(savingWords, codeEncode[indexRow].wordsOfRow[lableIndex]) &&
		!nameInArrayChar((char**)valdiRegister, codeEncode[indexRow].wordsOfRow[lableIndex]) &&
		codeEncode[indexRow].wordsOfRow[lableIndex][0] != '#') /*the parameter is a lable*/
	{
		if (codeEncode[indexRow].wordsOfRow[lableIndex][0] == '&') /*address methode number 2*/
			lableOffset++;

		while (head)
		{
			if (!strcmp(head->data.sign, codeEncode[indexRow].wordsOfRow[lableIndex] + lableOffset)) /*lable is found*/
			{
				if(lableOffset) /*address methode number 2*/
					strcpy(codeEncode[indexRow].encode[originParamInd],
						convertToBase2(head->data.address - codeEncode[indexRow].memoryCell));
				else /*methode adress number 1*/
					strcpy(codeEncode[indexRow].encode[originParamInd],convertToBase2(head->data.address));
				for (i = 3; i < 25; i++)
					codeEncode[indexRow].encode[originParamInd][i - 3] = codeEncode[indexRow].encode[originParamInd][i];

				/*update the ARE bits according to the lable status*/
				if (head->data.exterOrEntry != external && !lableOffset) /*R for lable that his definition
																		 is in this cuurent file*/
				{
					codeEncode[indexRow].encode[originParamInd][21] = '0';
					codeEncode[indexRow].encode[originParamInd][22] = '1';
					codeEncode[indexRow].encode[originParamInd][23] = '0';
				}
				else if (head->data.exterOrEntry == external) /*E bit of external lable*/
				{
					codeEncode[indexRow].encode[originParamInd][21] = '0';
					codeEncode[indexRow].encode[originParamInd][22] = '0';
					codeEncode[indexRow].encode[originParamInd][23] = '1';

					/*write the lable to externals files*/
					fprintf(externFile, "%s 000%d \n", codeEncode[indexRow].wordsOfRow[lableIndex + lableOffset],
						codeEncode[indexRow].memoryCell + originParamInd); /*the sign and his IC*/
				}
				else if(lableOffset) /*distance is always with A bit*/
				{
					codeEncode[indexRow].encode[originParamInd][21] = '1';
					codeEncode[indexRow].encode[originParamInd][22] = '0';
					codeEncode[indexRow].encode[originParamInd][23] = '0';
				}
				break;
			}

			head = head->next;
		}

		if (!head)
		{
			if (isNum(codeEncode[indexRow].wordsOfRow[lableIndex] + lableOffset))
				appendTextToArray(errors, "number parameter without # is not legal", codeEncode[indexRow].rowNumber);
			else
				appendTextToArray(errors, "the symbol don't exist in the lable table \n",codeEncode[indexRow].rowNumber);
			return false;
		}
	}

	return true;
}

/*
update the encoding of the two parameters row

input: index - the index of the row
ouput: 1 - the encoding his done successfullu
	   0 -  there was something wrong
*/
int updateEncodeTwoParams(int index)
{
	boolean hasLable = false;
	if (codeEncode[index].wordsOfRow[0][strlen(codeEncode[index].wordsOfRow[0]) - 1] == ':')
		hasLable = true;
	/*true - only if the bot parameters are fine*/
	return getLableAddress(1, index, hasLable) && getLableAddress(2, index, hasLable);
}

/*
update the encoding of the one parameters row

input: index - the index of the row
ouput: 1 - the encoding his done successfullu
	   0 -  there was something wrong
*/
int updateEncodeOneParam(int index)
{
	boolean hasLable = false;
	if (codeEncode[index].wordsOfRow[0][strlen(codeEncode[index].wordsOfRow[0]) - 1] == ':')
		hasLable = true;
	return getLableAddress(1, index, hasLable);
}
