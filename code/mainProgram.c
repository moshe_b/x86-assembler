#include "dataStructures.h"
#include "InputAnalysis.h"
#include"checkValidInputHelpFunctions.h"
#include"secondPass.h"
#include"addingToDataStructures.h"
#include"mainProgram.h"
#include"encodingToDataStructures.h"

FILE* entryFile = 0;
FILE* externFile = 0;
void restartDataStructures();

int main(int argc, char** argv)
{
	int i = 1;
	ROW_DATA rowData;
	for(; i < argc; i++)
        {
		fullPass(argc, argv, i);
		addEncode('e',rowData);
		restartDataStructures();
	}

	return 0;
}

/*
the function of the full program.
contain the two pass of specific file
from the program
*/
void fullPass(int argc, char** argv,int indFile)
{
	FILE* assamblyCodeFile = 0;
	FILE* assamblyEncodeFile = 0;
	int i = 0;
	char fileName[FILE_NAME_SIZE] = { 0 };

	strcpy(fileName, argv[indFile]);
	assamblyCodeFile = fopen(argv[indFile], "r");
	/*open the entry file and the extern file*/
	externFile = fopen(strcat(fileName, ".ext"), "w"); /*.ext*/

	for (i = 0; i < FILE_NAME_SIZE; i++) fileName[i] = 0;
	strcpy(fileName, argv[indFile]);
	
	entryFile = fopen(strcat(fileName, ".ent"), "w"); /*.ent*/
	if (!assamblyCodeFile || !externFile || !entryFile)
	{
		fprintf(stderr, "cannot open the file \n");
		getchar();
		exit(1);
	}
	firstPass(assamblyCodeFile); /*the first pass*/
	i = 0;
	fseek(assamblyCodeFile, 0, SEEK_SET); /*go back to the begining of the file*/
	if (errors[0][0] == ERROR_END_SIGN || onlyWarnings()) /*in the first pass there is no problems*/
	{
		updateLablesTable(IC); /*here IC is ICF*/
		updateDataEncode(IC);
		updateSecondPass(assamblyCodeFile);
		if (errors[0][0] == ERROR_END_SIGN || onlyWarnings()) /*there is no errors in second pass*/
	{
			for (i = 0; i < FILE_NAME_SIZE; i++) fileName[i] = 0;
			strcpy(fileName, argv[indFile]);
			assamblyEncodeFile = fopen(strcat(fileName, ".ob"), "w"); /*.ob*/
			writingAssamblyFile(assamblyEncodeFile); /*writing teh assambly file encoding*/
			fclose(assamblyCodeFile);
			fclose(assamblyEncodeFile);

			/*print it if there is warnings in the errors*/
			i = 0;
			while (errors[i][0] != ERROR_END_SIGN)
			{
				printf("%s\n", errors[i]);
				i++;
			}
			
		}
		else
		{
			i = 0;

			/*printing errors file*/
			while (errors[i][0] != ERROR_END_SIGN)
			{
				printf("%s\n", errors[i]);
				i++;
			}
			for (i = 0; i < FILE_NAME_SIZE; i++) fileName[i] = 0;
			strcpy(fileName, argv[indFile]);
			remove(strcat(fileName, ".ext"));
			for (i = 0; i < FILE_NAME_SIZE; i++) fileName[i] = 0;
			strcpy(fileName, argv[indFile]);
			remove(strcat(fileName, ".ent"));
		}
	}
	else
	{
		i = 0;
		while (errors[i][0] != ERROR_END_SIGN)
		{
			printf("%s\n", errors[i]);
			i++;
		}
		for (i = 0; i < FILE_NAME_SIZE; i++)
			fileName[i] = 0;
		strcpy(fileName, argv[indFile]);
		remove(strcat(fileName, ".ext"));
		for (i = 0; i < FILE_NAME_SIZE; i++)
			fileName[i] = 0;
		strcpy(fileName, argv[indFile]);
		remove(strcat(fileName, ".ent"));
		
	}

	rewind(entryFile);
	rewind(externFile);
	/*check if entry and extern file are empties.
	if they does I remove them*/
	for (i = 0; i < FILE_NAME_SIZE; i++)
		fileName[i] = 0;
	strcpy(fileName, argv[indFile]);
	if (fgetc(entryFile) != EOF)
		remove(strcat(fileName, ".ent"));
	for (i = 0; i < FILE_NAME_SIZE; i++) fileName[i] = 0;
		strcpy(fileName, argv[indFile]);
	if (fgetc(externFile) != EOF)
		remove(strcat(fileName, ".ext"));
}

/*
get the path of the file
without the name of the file

input: pathPlusName - the full path
	   including the name
output: path - the path without the name
*/
char* getPath(char* pathPlusName)
{
	int i = 0;
	char* path = (char*)calloc(SIZE_PATH, sizeof(char));
	for (i = strlen(pathPlusName) - 1; i >= 0; i--)
	{
		if (pathPlusName[i] == '\\')
			break;
		else
			path[i] = pathPlusName[i];
	}

	return strncpy(path,pathPlusName,i);
}

void printCodeData()
{
	int i = 0;
	int j = 0;
	while (codeEncode[i].wordsOfRow[0][0] != 0)
	{
		puts(codeEncode[i].wordsOfRow[0]);
		printf("DC = %d\n", codeEncode[i].memoryCell);
		printf("L = %d \n", codeEncode[i].L);
		for (j = 0; j < codeEncode[i].L; j++)
			puts(codeEncode[i].encode[j]);
		i++;
	}
	i = 0;
	while (dataEncode[i].encode[0][0] != 0)
	{
		puts("aa");
		for (j = 0; j < dataEncode[i].L; j++)
			puts(dataEncode[i].encode[j]);
		i++;
	}

	
}

void printLableTable()
{
	SYMBOLES_TABLE_NODE*  head = 0;
	head = labelTable;

	while (head)
	{
		printf("%s = %d\n", head->data.sign, head->data.address);
		head = head->next;
	}
}

int onlyWarnings()
{
	int i = 0;

	while (strstr(errors[i], "warning") && strcmp("%",errors[i]))
		i++;
	return !strcmp("%", errors[i]);
}

void restartDataStructures()
{
	int i = 0;
	int j = 0;
	int k = 0;
	SYMBOLES_TABLE_NODE* head = 0;
	SYMBOLES_TABLE_NODE* ptr = 0;

	IC = START_MEMORY_ADDRESS;
	DC = 0;
	rowCounter = 1;
	head = labelTable;

	while (head->next != 0)
	{
		ptr = head->next;
		free(head);
		head = ptr;
		ptr  = 0;
	}

	free(head);
	labelTable = 0;

	while (errors[i][0] != '%')
	{
		for (j = 0; j < strlen(errors[i]); j++)
			errors[i][j] = 0;
		i++;
	}
        errors[0][0] = '%';
	i = 0;
	j = 0;
	while (codeEncode[i].memoryCell != 0)
	{
		for (j = 0; j < codeEncode[i].L; j++)
			for (k = 0; k < 25; k++)
			{
				codeEncode[i].encode[j][k] = 0;
			}
	

		j = 0;
		while (codeEncode[i].wordsOfRow[j][0] != 0)
		{
			for (k = 0; codeEncode[i].wordsOfRow[j][0] != 0; k++)
			{
				codeEncode[i].wordsOfRow[j][k] = 0;
			}

			j++;
		}

		codeEncode[i].L = 0;
		codeEncode[i].rowNumber = 0;
		codeEncode[i].memoryCell = 0;
		i++;
	}

	i = 0;
	j = 0;
	while (dataEncode[i].memoryCell != 0)
	{
		for (j = 0; j < dataEncode[i].L; j++)
			for (k = 0; k < 25; k++)
			{
			      dataEncode[i].encode[j][k] = 0;
			}
	

		j = 0;
		while (dataEncode[i].wordsOfRow[j][0] != 0)
		{
			for (k = 0; dataEncode[i].wordsOfRow[j][k] != 0; k++)
			{
				dataEncode[i].wordsOfRow[j][k] = 0;
			}

		}	j++;


		dataEncode[i].L = 0;
		dataEncode[i].rowNumber = 0;
		dataEncode[i].memoryCell = 0;
		i++;
	}

}
