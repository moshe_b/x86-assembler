#ifndef ADDING
#define ADDING
#include "dataStructures.h"
#include<stdlib.h>

extern int IC;
extern int DC;
extern SYMBOLES_TABLE_NODE* labelTable;
extern char errors[80][200];
extern char valdiRegister[9][3];
extern char* savingWords[22];

void insertAtEnd(SYMBOLES_TABLE_NODE** lableTableParam, SYMBOLES_TABLE_NODE* lableToAdd);
SYMBOLES_TABLE_NODE* createSymboleNode(char sign[], int address, int instructionOrAction, int exter);
int addLable(char** rowSplitedData);
int addLableExtern(char** rowSplitedData, int lableInd);

#define LABLE_SIZE 100
#define MAX_LABLE_SIZE 31

#endif

