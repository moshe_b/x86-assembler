#include"dataStructures.h"
#include"checkValidInputHelpFunctions.h"
#include"checkValidInput.h"
#include"encodingToDataStructures.h"
#include"InputAnalysis.h"
#include"parseBases.h"
#include"addingToDataStructures.h"
#include"secondPass.h"


/*
update every thing for the second pass.
the entry rows and continue encoding all
the lables

input: assambllyFile - the asamblly file for reading
from him
*/
void updateSecondPass(FILE* assambllyFile)
{
	char rowData[MAX_LENGTH_ROW] = { 0 };
	char** splitedRowEntry = 0;
	char** splitedRow = 0;
	int i = 0;
	int j = 0;

	rowCounter = 0;
	/*update the sybol table of every entry lable*/
	while (fgets(rowData, 80, assambllyFile) != NULL)
	{
		if (rowData[0] == ';' || emptyString(rowData)) /*remarks so continue to the next row*/
		{
			rowCounter++;
			continue;
		}
		splitedRowEntry = splitByAsterisk(rowData);
		if (findNameCommand(splitedRowEntry) == entry)
			checkValidEntry(splitedRowEntry);
		rowCounter++;
	}
        j = 0;
	/*update the encoding of the labels*/
	/*code: */
	while (codeEncode[i].memoryCell != 0) /*global data structure so the memory cell must be 0 at teh end*/
	{
		splitedRow = (char**)malloc(20*sizeof(char*));
		j = 0;
		while (codeEncode[i].wordsOfRow[j][0] != 0)
		{
			splitedRow[j] = (char*)malloc(100*sizeof(char));
			strcpy(splitedRow[j], codeEncode[i].wordsOfRow[j]);
			j++;
		}
		splitedRow[j] = (char*)malloc(25*sizeof(char));
		splitedRow[j][0] = '%';

		/*update according the type of name of command*/
		switch (findNameCommand(splitedRow))
		{
			case mov:
			case cmp:
			case add:
			case sub:
			case lea:
				updateEncodeTwoParams(i); /*i for the index of update*/
				break;
			case clr:
			case not:
			case inc:
			case dec:
			case jmp:
			case bne:
			case jsr:
			case red:
			case prn:
				updateEncodeOneParam(i);
				break;
		}

		j = 0;
		/*while (splitedRow[j][0] != '%')
		{
			free(splitedRow[j]);
			j++;
		}
		free(splitedRow[j]);
		free(splitedRow);*/
		i++;
	}
}

/*
writing to the object file.

input: assamblyFile - the assambly file it self
output: None
*/
void writingAssamblyFile(FILE* assamblyFile)
{
	int i = 0;
	int j = 0;
	int k = 0;
	char zeros[7] = { 0 };
	char num[7] = { 0 };
	
	/*the size of the action and the instruction*/
	fprintf(assamblyFile,"    %d  %d\n", IC - 100, DC); 

	/*
	writing the code part
	*/
	while (codeEncode[i].wordsOfRow[0][0] != 0)
	{
		for (k = 0; k < codeEncode[i].L; k++)
		{
			sprintf(num, "%d", codeEncode[i].memoryCell + k);
			for (j = 0; j < 7 - strlen(num); j++)
				zeros[j] = '0';
			if(zeros[0] == 0)
				fprintf(assamblyFile, "%s %s \n", num,Base2ToBase16(codeEncode[i].encode[k]));
			else
				fprintf(assamblyFile, "%s%s %s\n", zeros,num,Base2ToBase16(codeEncode[i].encode[k]));
		}
		i++;
	}

	i = 0;
	/*
	writing the data part
	*/
	while (dataEncode[i].L != 0)
	{
		for (k = 0; k < dataEncode[i].L; k++)
		{
			sprintf(num, "%d", dataEncode[i].memoryCell + k);
			for (j = 0; (unsigned)j < 7 - strlen(num); j++)
				zeros[j] = '0';

			if (zeros[0] == 0)
				fprintf(assamblyFile, "%s %s\n",num, Base2ToBase16(dataEncode[i].encode[k]));
			else
				fprintf(assamblyFile, "%s%s %s\n",zeros,num,Base2ToBase16(dataEncode[i].encode[k]));
		}
		i++;
	}
}


/*
update the lable table and add 
to it the ICF.

input: ICF - the final IC
output: None
*/
void updateLablesTable(int ICF)
{
	SYMBOLES_TABLE_NODE* head = 0;
	head = labelTable;
	while (head)
	{
		if (head->data.instructOrAction == instruction)
			head->data.address += ICF;
		head = head->next;
	}
}

/*
update the memory cell of every
data lable to be the origne DC+ICF

input: ICF - the final ICF
output: None
*/
void updateDataEncode(int ICF)
{
	int i = 0;
	while (dataEncode[i].L != 0)
		dataEncode[i++].memoryCell += ICF;
}
