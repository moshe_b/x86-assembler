#ifndef MAIN_PROGRAM
#define MAIN_PROGRAM
#include "dataStructures.h"
#include "InputAnalysis.h"
#include"checkValidInputHelpFunctions.h"
#include"secondPass.h"
#include"addingToDataStructures.h"
#include<stdlib.h>
extern char errors[80][200];
char* convertToBase2(int numToConvert);
char* Base2ToBase16(char* base2);
extern char* savingWords[22];
extern SYMBOLES_TABLE_NODE* labelTable;
extern ROW_DATA dataEncode[DATA_SIZE];
extern ROW_DATA codeEncode[CODE_SIZE];
extern int DC;
extern int IC;

void firstPass(FILE* assablyFile);
void updateLablesTable(int ICF);
void updateDataEncode(int ICF);
void printCodeData();
void printLableTable();
void fullPass(int argc, char** argv, int indFile);
int onlyWarnings();

#define FILE_NAME_SIZE 30
#define SIZE_PATH 80
#define ERROR_END_SIGN '%'
#endif 

