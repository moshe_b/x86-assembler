#include"dataStructures.h"
#include"checkValidInputHelpFunctions.h"
#include"checkValidInput.h"
#include"encodingToDataStructures.h"
#include"InputAnalysis.h"
#include"parseBases.h"
#include"addingToDataStructures.h"

extern char errors[80][200];
extern int rowCounter;
void printSplitedRow(char** splitedRow);


/*
the function for the first pass.
read all the rows and check the valid
of the syntax and all the methode address
and encode what she can in the first pass.

input: assamblyFile - the assambly file
output: none
*/
void firstPass(FILE* assablyFile)
{
	char rowData[200] = { 0 };
	char** splitedRow = 0;
	
	/*read all the rows  from the assmaby file*/
	while (fgets(rowData, 200, assablyFile) != NULL)
	{

		if (rowData[0] == ';' || emptyString(rowData)) /*remarks so continue to the next row*/
		{
			rowCounter++;
			continue;
		}

		if (strlen(rowData) > 81)
		{
			appendTextToArray(errors, "row length is too big, must be only 80 characters\n",rowCounter);
			rowCounter++;
			continue;
		}
		splitedRow = splitByAsterisk(rowData);
		/*switch for all the kind of the commands*/
		switch (findNameCommand(splitedRow))
		{
			case mov:
			case cmp:
			case add:
			case sub:
			case lea: 
				checkValidTwoParams(splitedRow);
				break;
			case clr:
			case not:
			case inc:
			case dec:
			case jmp:
			case bne:
			case jsr:
			case red:
			case prn:
				checkValidOneParam(splitedRow);
				break;
			case rts:
			case stop:
				checkValidZeroParam(splitedRow);
				break;
			case data:
				checkValidData(splitedRow);
				break;
			case string:
				checkValidString(splitedRow);
				break;
			case entry:
				rowCounter++;
				continue; /*leaving to the second pass*/
				break;
			case external:
				checkValidExtern(splitedRow);
				break;
			default:
				appendTextToArray(errors, "unknown name of command \n",rowCounter);

		}
		rowCounter++;
	}
}

void printSplitedRow(char** splitedRow)
{
	int i = 0;
	while (splitedRow[i][0] != '%')
	{
		printf("%s ", splitedRow[i]);
	}
	printf("\n");
}
