#ifndef PARSE_BASES
#define PARSE_BASES

#include<stdlib.h>
#define SIZE_WORD 25 /*24 + NULL*/
#define ZERO_ASCII_VAL '0'
#define BASE_2 2
#define SIZE_WORD_BASE_16 6

char* convertToBase2(int numToConvert);
char* Base2ToBase16(char* base2);
#endif

