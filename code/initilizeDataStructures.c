#include "dataStructures.h"


/*opcode funct srcAddress, destAddress*/
STRUCT_COMMAND commandWordsData[NUM_COMMAND_WORDS] = { {0,-1,{0,1,3},{1,3,-1}}, /*mov*/
										{1,-1,{0,1,3},{0,1,3 }}, /*cmp*/
										{2,1,{0,1,3},{1,3,-1}}, /*add*/
					     				{2,2,{0,1,3},{1,3,-1}}, /*sub*/
				    					{4,-1,{1,-1,-1},{1,3,-1}}, /*lea*/
										{5,1,{-1,-1,-1},{1,3,-1}}, /*clr*/
										{5,2,{-1,-1,-1},{1,3,-1}}, /*not*/
										{5,3,{-1,-1,-1},{1,3,-1}}, /*inc*/
										{5,4,{-1,-1,-1},{1,3,-1}}, /*dec*/
										{9,1,{-1,-1,-1},{1,2,-1}}, /*jmp*/
										{9,2,{-1,-1,-1},{1,2,-1}}, /*bne*/
										{9,3,{-1,-1,-1},{1,2,-1}}, /*jsr*/
									    {12,-1,{-1,-1,-1},{1,3,-1}}, /*red*/
									 	{13,-1,{-1,-1,-1},{0,1,3}}, /*prn*/
										{14,-1,{-1,-1,-1},{-1,-1,-1}}, /*rts*/
										{15,-1,{-1,-1,-1},{-1,-1,-1}} }; /*stop*/ 

ROW_DATA dataEncode[DATA_SIZE];
ROW_DATA codeEncode[CODE_SIZE];
SYMBOLES_TABLE_NODE* labelTable;
char errors[SIZE_ERRORS][LENGTH_ERROR] = { {'%',0} };

/*register valid name*/
char* valdiRegister[NUMBER_REGISTERS] = { "r0","r1","r2","r3","r4","r5","r6","r7","%"};
/*all the saving words in the assambly language*/
char* savingWords[NUMBER_SAVING_WORDS] = { "mov","cmp","add","sub","lea","clr","not","inc"
										  ,"dec","jmp","bne","jsr","red","prn","rts","stop",
										  ".data",".string",".entry",".extern","%"};

int DC = 0;
int IC = START_MEMORY_ADDRESS;
int rowCounter = 1; /*the row counter for the addoing to the errors*/
