/*
this source file will contain
all the mini functions that check 
every command or data word.
for example, fucntion for add, sub,
.string, .entry, stop...
*/

#include "dataStructures.h"
#include "checkValidInputHelpFunctions.h"
#include "encodingToDataStructures.h"
#include "addingToDataStructures.h"
#include "checkValidInput.h"


/*
the function check the valid 
of the current row in the assambly
file.

input: rowSplitedData - the splited row
that split by asterisk and space.
output: true = 1 -> the row is valid
        false = 0 -> the row is not valid
*/
int checkValidTwoParams(char** rowSplitedData)
{
	int indName = 0;
	int* commasIndexs = 0;
	int* textIndex = 0; /*all string that start with ascii val(not comma)*/
	boolean hasLable = false; /*lable flage, false - there is no lable, true - there is a lable is the row*/
	indName = nameCommandIndex(rowSplitedData); /*find the index of command */
	commasIndexs = indexesText(rowSplitedData, COMMA_SIGN_STR);
	textIndex = indexesNoneCommaNoneLable(rowSplitedData); /*find all teh index of the none lable and lable*/

	/*check valid of the label*/
	if (addLable(rowSplitedData))  /*there is lable and he is valid*/
	{
		hasLable = true;		
	}
	/*check valid of the commas*/
	
	if (textIndex[0] < indName) /*there is text before name command*/
	{
		appendTextToArray(errors ,"Text before name command\n",rowCounter);
	}
	/*all text is after name of command*/
	else if (commasIndexs[0] < indName)
	{
		appendTextToArray(errors, "comma befoe name of command\n",rowCounter);
	}
	else if (lenArr(textIndex) > 2) /*text after end of legal command*/
	{
		appendTextToArray(errors, "text after last parameter \n",rowCounter);
	}
	else if (existInRange(commasIndexs, indName, textIndex[0])) /*comma exist between name of command to first parameter*/
	{
		appendTextToArray(errors, "comma between name to first parameter\n",rowCounter);
	}
	else if ((textIndex[0] - indName) == 0) /*text exist between the name to the first parameter*/
	{
		appendTextToArray(errors, "text between name to first parameter \n",rowCounter);
	}
	else if (lenArr(commasIndexs) >= 2)
	{
		if (commasIndexs[1] > textIndex[0] && commasIndexs[1] < textIndex[1] &&
			commasIndexs[0] > textIndex[0] && commasIndexs[0] < textIndex[1])
			appendTextToArray(errors, "duplicate comma between two parameters \n",rowCounter);
	}
	else if (!existInRange(commasIndexs, textIndex[0], textIndex[1])) /*comma exist between two parameters*/
	{
		appendTextToArray(errors, "comma between first parameter to second parameter must exists\n",rowCounter);
	}
	else if (textIndex[1] - textIndex[0] > 2) /*2 - including the comma between the parameters*/
	{
		appendTextToArray(errors, "text between two parameters \n",rowCounter);
	}
	/*here all the "synatx" if fine*/
	else 
	{
		/*if row is valid the data will be encoding for the first pass*/
		return checkValid2Params(rowSplitedData, commandIntVal(rowSplitedData[indName]), hasLable);
	}

	return false;
}


/*
check valid of action sentenses
that contained 1 parameter.

input: rowSplitedData - the splited row
output: true - the sentense syntax for the first pass is correct
		false - otherwise
*/
int checkValidOneParam(char** splitedRow)
{
	int indName = 0;
	int* commasIndexs = 0;
	int* textIndex = 0; /*all string that start with ascii val(not comma)*/
	boolean hasLable = false; /*lable flage, false - there is no lable, true - there is a lable is the row*/

	indName = nameCommandIndex(splitedRow); /*find the index of command */
	commasIndexs = indexesText(splitedRow, COMMA_SIGN_STR);
	textIndex = indexesNoneCommaNoneLable(splitedRow); /*find all teh index of the none lable and lable*/

	/*check valid of the label*/
	if (addLable(splitedRow))  /*there is lable and he is valid*/
	{
		hasLable = true;
	}

	if (lenArr(textIndex) > 1) /*check if there is text before name of command*/
		appendTextToArray(errors, "to many parameters \n", rowCounter);
	else if (lenArr(textIndex) == 0) /*suppose to be 1 for the dest parameter*/
		appendTextToArray(errors, "missing parameter \n",rowCounter);
	else if (textIndex[0] < indName)
		appendTextToArray(errors, "text before name of command \n",rowCounter);
	else if (commasIndexs[0] < indName && commasIndexs[0] != -1)
		appendTextToArray(errors, "comma before name of comamnd is ilegal \n",rowCounter);
	else if (existInRange(commasIndexs, indName, textIndex[0]))
		appendTextToArray(errors, "comma between name of command and the parameter \n",rowCounter);
	else /*all the syntax row is fine*/
	{
		return checkValid1ParamCommand(splitedRow, commandIntVal(splitedRow[indName]), hasLable);
	}

	return false; /*something is wrong with the row*/
}

/*
check valid of zero parameters
of action sentense.

input: rowSplitedData - the splited row from
	   the assambly file
output: true - the row syntax for the first pass is fine
		false - otherwise
*/
int checkValidZeroParam(char** rowSplited)
{
	int startIndex = 0;
	boolean hasLable = false;
	int nameCommandIndexVar = 0;
	int* commasIndexes = 0;
	int* textIndexes = 0;

	/*check if there is lable and 
	add it if it's syntax is correct*/
	if (addLable(rowSplited))
	{
		startIndex = 1;
		hasLable = true;
	}

	nameCommandIndexVar = nameCommandIndex(rowSplited); /*find the index of command */
	textIndexes = indexesNoneCommaNoneLable(rowSplited);
	commasIndexes = indexesText(rowSplited, COMMA_SIGN_STR);

	if (nameCommandIndexVar - startIndex > 1) /*there is text or commas before name of command*/
	{
		if (textIndexes[0] < nameCommandIndexVar)
			appendTextToArray(errors, "text before name of command is ilegal \n",rowCounter);
		else if (commasIndexes[0] < nameCommandIndexVar)
			appendTextToArray(errors, "comma before name of command is ilegal \n",rowCounter);
	}
	else if (textIndexes[0] > nameCommandIndexVar)
		appendTextToArray(errors, "text after name of command is ilegal \n",rowCounter);
	else if (commasIndexes[0] > nameCommandIndexVar)
		appendTextToArray(errors, "comma after name of command is ilegal \n",rowCounter);
	else/*everything is fine with the syntax of the row*/
	{
		encodeZeroParam(rowSplited, hasLable);
		return true;
	}

	return false;
}


/*
check valid of string sentense

input: rowSplitedData - the splited row
output: true - the syntax of the row is correct
		false - otherwise
*/
int checkValidString(char** rowSplited)
{
	boolean hasLable = false;
	int nameCommandInd = 0;
	int stringIndex = 0;
	int* commasIndexes = 0;
	int* textIndexes = 0;

	/*check if there is lable definition and add
	id to the lable table if it's syntax is fine*/
	if (addLable(rowSplited))
	{
		hasLable = true;
	}

	stringIndex = findStringIndex(rowSplited);
	nameCommandInd = nameCommandIndex(rowSplited);
	textIndexes = indexesNoneCommaNoneLable(rowSplited); /*contain the string*/
	commasIndexes = indexesText(rowSplited, COMMA_SIGN_STR);

	/*check valid of string*/
	if (stringIndex == -1)
	{
		appendTextToArray(errors, "must be string in .string row with openning and closing quatation marks\n", rowCounter);
	}
	else if (stringIndex < nameCommandInd)
		appendTextToArray(errors, "string must be after .stirng, not before! \n",rowCounter);
	else if (textIndexes[0] < nameCommandInd && textIndexes[0] != -1)
		appendTextToArray(errors, "text before name of command is ilegal \n",rowCounter);
	else if (commasIndexes[0] < nameCommandInd && commasIndexes[0] != -1)
		appendTextToArray(errors, "comma before name of command is ilegal \n",rowCounter);
	else if (lenArr(textIndexes) != 0)
	{
		if (stringIndex - nameCommandInd > 1)
		{
			if (textIndexes[0] > nameCommandInd && textIndexes[0] < stringIndex) /*text before parameter*/
			{
				appendTextToArray(errors, "text between .string to \"string\" itself is not allowed \n", rowCounter);
			}
			else if (textIndexes[0] > stringIndex)
				appendTextToArray(errors, "text after string is ilegal \n", rowCounter);
			else if (commasIndexes[0] > nameCommandInd)
				appendTextToArray(errors, "comma before string is ilegal \n",rowCounter);
			else if(commasIndexes[0] > stringIndex)
				appendTextToArray(errors, "comma after string is ilegal \n", rowCounter);
		}
		else
		{
			if (textIndexes[0] > stringIndex)
				appendTextToArray(errors, "text after string is ilegal \n",rowCounter);
			else if (commasIndexes[0] > stringIndex)
				appendTextToArray(errors, "comma after string is ilegal \n",rowCounter);
		}
	}
	else /*every thing is fine with the command .string and now you can encode it*/
	{
		encodeStringRow(rowSplited, hasLable);
		return true;
	}
	
	return false;
}

/*
check valid of data sentense.

input: splitedRow - the splited row from the assambly file
output: true - the data sentense syntax is fine
		false - otherwise
*/
int checkValidData(char** splitedRow)
{
	int dataIndex = 0;
	int* commasIndexes = 0;
	int* textIndexes = 0;
	
	dataIndex = nameCommandIndex(splitedRow); /*find the index of command */
	commasIndexes = indexesText(splitedRow, COMMA_SIGN_STR);
	textIndexes = indexesNoneCommaNoneLable(splitedRow);

	/*check if there is lable defintion
	and add it to the lable table if the syntax is fine*/
	addLable(splitedRow);

	/*cehck valid of evertything before the numbers*/
	if (textIndexes[0] < dataIndex)
		appendTextToArray(errors, "text before \".data\" is ilegal \n",rowCounter);
	else if (commasIndexes[0] < dataIndex && commasIndexes[0] != -1)
		appendTextToArray(errors, "comma before \".data\" is ilegal \n",rowCounter);
	else if (commasIndexes[0] > dataIndex && commasIndexes[0] < textIndexes[0] && commasIndexes[0] != -1)
		appendTextToArray(errors, "comma after \"data\" and before numbers is ilegal \n",rowCounter);
	else if (textIndexes[0] > dataIndex && !isdigit(splitedRow[textIndexes[0]][0]) && splitedRow[textIndexes[0]][0] != '-' &&
		splitedRow[textIndexes[0]][0] != '+')
		appendTextToArray(errors, "ilegal text after \".data\" \n",rowCounter);
	else if (textIndexes[0] > dataIndex) /*from here we assume that is fine and the
										next function will check if the syntext is fine and after it
										she will encode the numbers*/
	{
		if (dataNumbersEncode(splitedRow + textIndexes[0])) /*encode the number if every thing is fine*/
			return true;
	}
		
	return false;
}

/*
check valid of extern sentense.

input: splitedRow - the splited row
output: true - the syntax is fine
		false - otherwise
*/
int checkValidExtern(char** splitedRow)
{
	int nameCommandInd = 0;
	int* commasIndexes = 0;
	int* textIndexs = 0;

	nameCommandInd = nameCommandIndex(splitedRow);
	commasIndexes = indexesText(splitedRow, COMMA_SIGN_STR);
	textIndexs = indexesNoneCommaNoneLable(splitedRow);

	/*
	all the checking of the syntax
	*/
	if (textIndexs[0] < nameCommandInd && textIndexs[0] != -1)
		appendTextToArray(errors, "text before \".extern\" is ilegal \n",rowCounter);
	else if (commasIndexes[0] < nameCommandInd && commasIndexes[0] != -1)
		appendTextToArray(errors, "commas before \".extern\" is ilegal \n",rowCounter);
	else if (lenArr(textIndexs) > 2)
	{
		if (textIndexs[0] > nameCommandInd)
			appendTextToArray(errors, "text after the lable is not allowed \n",rowCounter);
	}
	else if (lenArr(commasIndexes) > 2)
	{
		if (commasIndexes[0] < textIndexs[0] && commasIndexes[1] < textIndexs[0])
			appendTextToArray(errors, "duplicate commas between \"extern\" and the lable name \n",rowCounter);
	}
	else if (lenArr(commasIndexes) == 1)
	{
		if (commasIndexes[0] < textIndexs[0])
			appendTextToArray(errors, "comma before lable is not allowed in extern row \n",rowCounter);
		else if (commasIndexes[0] > textIndexs[0])
			appendTextToArray(errors, "comma after lable is not allowed in extern row \n",rowCounter);
	}
	else /*every thing is fine*/
	{
		/*definition of a lable in begining of extern 
		row is correct but you add warning*/
		if (splitedRow[0][strlen(splitedRow[0]) - 1] == ':')
		{
			appendTextToArray(errors, "warning: defenition of a lable in extern/entry row \n",rowCounter);
			if (addLableExtern(splitedRow, 2))
				return true;
		}
		else if (addLableExtern(splitedRow,1))
			return true;
	}
	return false;
}


/*
*********************
second pass check valid functions
*********************
*/


/*
check valid of entry sentense

input: splitedRow - the splitedRow from the 
	   assambly file
output: true - all the syntax is fine
		false - otherwise
*/
int checkValidEntry(char** splitedRow)
{
	SYMBOLES_TABLE_NODE* head = 0;
	int nameCommandInd = 0;
	int* commasIndexes = 0;
	int* textIndexs = 0;

	nameCommandInd = nameCommandIndex(splitedRow);
	commasIndexes = indexesText(splitedRow, COMMA_SIGN_STR);
	textIndexs = indexesNoneCommaNoneLable(splitedRow);

	/*
	check all the valid of the syntax
	*/
	if (textIndexs[0] < nameCommandInd)
		appendTextToArray(errors, "text before \".entry\" is ilegal \n",rowCounter);
	else if (commasIndexes[0] < nameCommandInd && commasIndexes[0] != -1)
		appendTextToArray(errors, "commas before \".entry\" is ilegal \n",rowCounter);
	else if (lenArr(textIndexs) > 1 && textIndexs[1] > nameCommandInd)
		appendTextToArray(errors, "text after the lable in entry sentense is ilegal \n",rowCounter);
	else if (commasIndexes[0] > nameCommandInd)
		appendTextToArray(errors, "text after \".entry\" is ilegal \n",rowCounter);
	else /*every thing is fine*/
	{
		/*definition of lable in entry sentense is need to add warning*/
		if (splitedRow[0][strlen(splitedRow[0]) - 1] == ':') 
			appendTextToArray(errors, "warning: defenition of a lable in extern/entry row \n",rowCounter);
		head = labelTable;

		/*update the lable to be enrty for the second pass*/
		while (head)
		{
			if (!strcmp(head->data.sign, splitedRow[nameCommandInd+1]))
			{
				head->data.exterOrEntry = entry;
				fprintf(entryFile, "%s 000%d \n", head->data.sign, head->data.address);
				 break;
			}
			head = head->next;
		}

		if (!head)/*entry lable must define already and be in the lable table*/
		{
			appendTextToArray(errors, "definition of entry lable don't exists \n",rowCounter);
			return false;
		}
                 return true;	
	}
	return false;
    
}
