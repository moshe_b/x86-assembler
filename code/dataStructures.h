#ifndef DATASTRUCTURE
#define DATASTRUCTURE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>


#define NUM_COMMAND_WORDS 16
#define CODE_SIZE 1000
#define DATA_SIZE 1000
#define SIZE_ERRORS 80
#define LENGTH_ERROR 200
#define NUMBER_SAVING_WORDS 22
#define NUMBER_REGISTERS 9
#define START_MEMORY_ADDRESS 100

/*bit fields for the encoding
the first word*/
typedef struct {
	unsigned int E : 1;
	unsigned int R : 1;
	unsigned int A : 1;
	unsigned int funct : 5;
	unsigned int destRegister : 3;
	unsigned int destAddress : 2;
	unsigned int sourceRegister : 3;
	unsigned int sourceAddress : 2;
	unsigned int opcode : 6;
	unsigned int : 0;
}bitfEncoding;

/*
 data structure for saving 
 the encoding of the row
*/
typedef struct
{
	char wordsOfRow[100][100]; /*2d array that contain the words of the 
							   row, the symbole, the command and the parameters.
							   in string and data she will contain minus sign for the second
							   pass. munus sign mean that we don't need to touch in the encoding*/
	int L; /*number of words for encoding*/
	char encode[80][25];  /*the encode itself, 3 words is the maximum
						   for the instruction rows and 80 is teh maximum for
						   the data rows, .string, .data*/
	int memoryCell; /*the current IC or DC*/
	int rowNumber; /*the row number in the file*/
}ROW_DATA;

/*necessary data for
  for the encoding actions
  words, add,sub,lea and so on...*/
typedef struct 
{
	int opcode;
	int fucnt;
	int srcAddressMethod[3];
	int dstAddressMethod[3];
}STRUCT_COMMAND;

enum
{
	entry=18, external=19, notExterEntry=70,instruction=80, action=90
};

/*symbols signs table data structure*/
typedef struct
{
	char sign[150]; /*the lable name itself*/
	int address; /*ID or DC we add 100 to all DC at the end*/
	int instructOrAction; /*1 - instruction 0 - action*/
	int exterOrEntry; /*the lable is entry or external or -1 when is not external and not entry*/
}SYMBOLS_TABLE;

/*node of one lable for creating the 
linked list*/
/*typedef struct SYMBOLES_TABLE_NODE;*/

typedef struct node
{
	SYMBOLS_TABLE data;
	struct node* next;
}SYMBOLES_TABLE_NODE;

typedef enum
{
	false, true
}boolean;


typedef enum {mov,cmp,add,sub,lea,clr,not,inc,dec,jmp,bne,jsr,red,prn,rts,stop, data, string}command_words;

typedef enum{intArr, charArr}type;
#endif


