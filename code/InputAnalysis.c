#include "InputAnalysis.h"

/*
the function will get string that contain
asterisk and she will split the string by
asterisk.

input: rowFile - the string to split
output: splitedWord - 2d array of the splited
       string.
*/
char** splitByAsterisk(char* rowFile)
{
	char** splitedWords = 0;
	char* token = 0;
	int i = 0;

	/*add in space and befoe and
	after comma asterisk*/
	rowFile = combineAsterisk(rowFile);  

	splitedWords = (char**)malloc(NUM_SPLITED_WORD*sizeof(char*));
	for (i = 0; i < NUM_SPLITED_WORD; i++)
	{
		splitedWords[i] = (char*)malloc(NUM_SPLITED_WORD);
	}

	/*split the string by asterisk*/
	token = strtok(rowFile, SPLITED_SIGN_STR);
	i = 0;

	/* walk through other tokens */
	while (token != NULL) {
		deletSideSpaces(&token);
		strcpy(splitedWords[i++],token);

		token = strtok(NULL, SPLITED_SIGN_STR);
	}

    splitedWords[i][0] = END_OF_WORDS_SIGN; /*sign for end of the string in 2d array*/

	return splitedWords;
}

/*
function will add asterisk
in every space and before
and after comma but no when
there is a string "string"

input: rowFile - string for add him 
       asterisks
ouptut: newRow - the string with 
        the asterisks
*/
char* combineAsterisk(char* rowFile)
{
	int indNewRow = 0;
	int ind = 0;
	int putAsterisk = 0;
	char* newRow = 0;
	int quotationMarksFirst = 0;
	deletSideSpaces(&rowFile);  /*delete spaces from the sides of the string*/
	newRow = (char*)malloc(sizeof(char) * 100);

	for (ind = 0; ind < strlen(rowFile); ind++, indNewRow++)
	{
		if (!quotationMarksFirst && rowFile[ind] == '"')
			quotationMarksFirst = 1;
		else if (quotationMarksFirst && rowFile[ind] == '"') /*closing quotation marks*/
		{
			/*zero if there is another string in the row*/
			quotationMarksFirst = 0;
		}

		if (!quotationMarksFirst && isspace(rowFile[ind]) && !putAsterisk)  /*space*/
		{
			newRow[indNewRow] = SPLITED_SIGN_CHAR;
			putAsterisk = 1;
		}
		else if (!quotationMarksFirst && rowFile[ind] == COMMA_SIGN && !putAsterisk)  /*comma and the previous char was none space*/
		{
			newRow[indNewRow] = SPLITED_SIGN_CHAR;
			newRow[++indNewRow] = rowFile[ind];
			newRow[++indNewRow] = SPLITED_SIGN_CHAR;
			putAsterisk = 1;
		}
		else if (!quotationMarksFirst && rowFile[ind] == COMMA_SIGN)  /*comma that the previous char was space*/
		{
			newRow[indNewRow] = rowFile[ind];
			newRow[++indNewRow] = SPLITED_SIGN_CHAR;
			putAsterisk = 1;
		}
		else if(!quotationMarksFirst && !isspace(rowFile[ind])) /*regelur sign*/
		{ 
			newRow[indNewRow] = rowFile[ind];
			putAsterisk = 0;
		}
		else /*space*/
		{
			newRow[indNewRow] = rowFile[ind];
		}
	}
	
	newRow[indNewRow] = '\0';  /*make string*/

	return newRow;
}

/*
delete the spaces in the 
side of the string.

input: str - the address of the 
       stirng str
output: none
*/
void deletSideSpaces(char** str)
{
	char* end = 0;
	char* begin = 0;
	int flagEnd = 0;

	begin = *str;
	end = &((*str)[strlen(*str) - 1]);

	while (isspace(*begin))  /*get first none space char*/
		begin++;
	while (isspace(*end))   /*get last none space char*/
	{
		*end = 0;
		end--;
		flagEnd = 1;
	}

	if(flagEnd)
		strncpy(begin, begin, end - begin);
	*str = begin;  /*change str to begin*/
}
